<?php

namespace App\Repositories\Contracts;

interface UserRepository {
    public function createAddress(int $user_id, array $properties);
    public function deleteAddress(int $user_id, int $address_id);
}
