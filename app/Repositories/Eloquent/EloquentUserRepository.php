<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\UserRepository;
use App\Repositories\RepositoryAbstract;
use App\User;

class EloquentUserRepository extends RepositoryAbstract implements UserRepository {
    public function entity()
    {
        return User::class;
    }

    public function createAddress($user_id, array $properties)
    {
        return $this->find($user_id)->addresses()->create($properties);
    }

    public function deleteAddress(int $user_id, int $address_id)
    {
        return $this->find($user_id)->addresses()->findOrFail($address_id)->delete();
    }
}
